const students = [
  { lastName: 'Laurent', firstName: 'Sacha', dateOfBirth: new Date('1958-10-07'), city: 'Nantes'},
  { lastName: 'Huet', firstName: 'Alicia', dateOfBirth: new Date('1965-04-22'), city: 'Argenteuil'},
  { lastName: 'Rousseau', firstName: 'Ambre', dateOfBirth: new Date('1982-08-07'), city: 'Perros-Guirrec'},
  { lastName: 'Charpentier', firstName: 'Thomas', dateOfBirth: new Date('1974-10-06'), city: 'Paris'},
  { lastName: 'Hettinger', firstName: 'Juana', dateOfBirth: new Date('1989-01-20'), city: 'Bourg-en-Bresse'},
  { lastName: 'Neri', firstName: 'Lazzaro', dateOfBirth: new Date('1983-10-07'), city: 'Caen'},
  { lastName: 'Marquardt', firstName: 'Eloise', dateOfBirth: new Date('1973-08-25'), city: 'Montpellier'},
  { lastName: 'Moen', firstName: 'Cornelius', dateOfBirth: new Date('1946-04-18'), city: 'Limoges'},
];

function studentClicked(student) {
  return function handle() {
    activeStudent = student;

    showStudents();
  }
}

function printStudent(student, table) {
  const row = table.insertRow();
  row.insertCell().textContent = student.firstName;
  row.insertCell().textContent = student.lastName;
  row.insertCell().textContent = student.dateOfBirth.toLocaleDateString();
  row.insertCell().textContent = student.city;
  row.style.backgroundColor = student === activeStudent ? 'red' : 'white';

  row.addEventListener('click', studentClicked(student));
}

function compareStudents (studentA, studentB) {
  return studentA.lastName.localeCompare(studentB.lastName);
}

let activeStudent = null;

function showStudents() {
  const table = document.querySelector('tbody');
  table.innerHTML = null;

  students.sort(compareStudents);

  students.forEach(function (student) {
    printStudent(student, table)
  });
}

document.addEventListener('DOMContentLoaded', showStudents);

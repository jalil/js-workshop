# Cheat sheet

## HTML Table

```html
<table>
  <caption>Table description</caption>
  <thead>
    <tr>
      <th>Column Title</th>
      <th>Another Column</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Some value for first line</td>
      <td>Another data</td>
    </tr>
    <tr>
      <td>Some value for second line</td>
      <td rowspan="2">Value for 2 row</td>
    </tr>
    <tr>
      <td>Some value for third line</td>
    </tr>
  </tbody>
</table>
```

## Simple text field

```html
<input type="text" name="fieldName"  />
```

## Multi line text field

```html
<textarea name="fieldName" />
```

## Local storage

Save data

```javascript
localStorage.setItem('someKey', 'someValue');
```

Read data

```javascript
localStorage.getItem('someKey');
```

